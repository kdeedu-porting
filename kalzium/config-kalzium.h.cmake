/* facile solver available? */
#cmakedefine HAVE_FACILE 1

/* Define to 1 if we have OpenBabel2 */
#cmakedefine HAVE_OPENBABEL2 1

/* Define 1 if we have OpenGL */
#cmakedefine HAVE_OPENGL 1

/* Define 1 if we have Eigen */
#cmakedefine HAVE_EIGEN 1

